// Import dependencies
const Webpack = require('webpack')
const Autoprefixer = require('autoprefixer')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

// Import common configuration
const common = require('./webpack.common')

// Make copy of common configuration
let config = Object.assign({}, common)

// Configure output
config.output =  {
  path: `${__dirname}/dist`,
  publicPath: '/',
  filename: '[name].[hash].js'
}

// Add production rules
config.module.rules = config.module.rules.concat([
  // Add sass compilation and extraction,
  // resolve relative urls, add autoprefixing,
  // and css modules
  {
    test: /\.(scss|sass)$/,
    use: ExtractTextPlugin.extract({
      use: [
        { 
          loader: 'css-loader',
          options: {
            modules: true,
            importLoaders: 3,
            localIdentName: '_[hash:base64:7]'
          }
        },
        { 
          loader: 'resolve-url-loader'
        },
        { 
          loader: 'postcss-loader',
          options: {
            plugins: [
              Autoprefixer({
                browsers: [
                  'last 2 versions', 
                  'ie >= 9', 
                  'Android >= 2.3', 
                  'ios >= 7'
                ]
              })
            ]
          }
        },
        { 
          loader: 'sass-loader'
        }
      ]
    })
  }
])

// Add production plugins
config.plugins = config.plugins.concat([
  // Tell loaders to minimize when possible
  new Webpack.LoaderOptionsPlugin({
    minimize: true
  }),
  // Configure uglification of js
  new Webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false,
      drop_console: true,
      screw_ie8: true
    },
    comments: false,
    sourceMap: false,
  }),
  // Define globals telling react
  // that we are in production mode
  new Webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production')
    },
  }),
  // Clean distribution folders
  new CleanWebpackPlugin(['dist']),
  // Extract css to file
  new ExtractTextPlugin({
    filename: '[name].[hash].css'
  })
])

// Export production configuration
module.exports = config