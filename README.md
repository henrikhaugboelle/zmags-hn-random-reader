![alt text](http://i.imgur.com/iH2x3E6.png "Presentation")

# Summary

Hi there. I have tried to keep the application short and concise, but without compromising structure and functionality.

The application is a classic React application with Redux. It fetches ten random stories using the Hacker News API and makes them available in the navigation so the user can shuffle through them. A preview is implemented, which also handles cases where the Hacker News post is lacking an url.

# A Word of Caution

As some websites do not allow iframes, the preview is sometimes blank – this could potentially be avoided using a server proxy.

# Demo

Demo can be [seen here](http://zmags.henrikhaugboelle.com/).

# Repository

Repository can be [found here](https://gitlab.com/henrikhaugboelle/zmags-hn-random-reader).

# Technology

Primary technology used to implement the application:

 - React
 - Redux
 - Babel / ES6
 - Sass
 - Foundation
 - Webpack

# Features

A few of the features I would like to emphasize:

- Navigation shows ten random stories by ascending score
- Preview for stories
- Preloaders
- Mobile responsive
- Uses modern JavaScript
- Uses Sass and local CSS modules
- Nice formatting of dates and points
- Homebrewn design (using zmags colors)

# Possible Improvements

- Implement proxy for websites that are denying being viewed through an iframe (but that's a bit too much outside the scope of the task)
- ... plus a ton more of course!

# Compatibility

The application has only been tested in latest releases of Chrome, Firefox, and Safari on macOS and Chrome on Android.

# Images

- [Showcase for URLS](http://i.imgur.com/VzWrw6Q.png)
- [Showcase for posts](http://i.imgur.com/T5H1brV.png)
- [Showcase for mobile navigation](http://i.imgur.com/4OsbW18.png)
- [Initial sketch](http://i.imgur.com/LClq4sB.jpg)