// Import dependencies
const Webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

// Export configuration
module.exports = {
  // We have two entry points
  entry: {
    // Vendor entry point
    vendor: [
      'react',
      'react-dom',
      'react-redux',
      'react-router',
      'react-transform-hmr',
      'redux',
      'redux-thunk',
      'whatwg-fetch',
      'classnames'
    ],
    // App entry point
    app: [
      './src/js/entry.js'
    ]
  },

  // Module
  module: {
    // Rules for module
    rules: [
      // Run js through babel loader (skip node modules)
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      // Use file loader for image files
      {
        test: /\.(jpe?g|png|gif|svg)?$/,
        use: 'file-loader' 
      },
      // Run css files through style, css, and resolve url loader
      {
        test: /\.(css)$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'resolve-url-loader',
            options: {
              keepQuery: true
            }
          }
        ]
      }
    ]
  },

  // Webpack plugins
  plugins: [
    // Use our custom index html template
    new HtmlWebpackPlugin({
      template: './src/html/index.html'
    }),
    // Separate vendor entry as chunk
    new Webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
    })
  ]
}