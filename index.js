// Super simple static file server

// Import dependencies
const Static = require('node-static')
const Http = require('http')

// Initialize static server
const fileServer = new Static.Server('./dist', {
  // Cache content a year
  cache: 60 * 60 * 24 * 364
})

// Initialize http server
const server = Http.createServer((request, response) => {
  request.addListener('end', () => {
    fileServer.serve(request, response)
  }).resume()
})

// Make server listen on port
server.listen(process.env.PORT || 3000)