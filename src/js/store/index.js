// Import dependencies
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

// Configure store method
export function configureStore(reducers, preloadedState = {}) {
  // Create redux store with reducers, preloaded state, and thunk
  const store = createStore(
    reducers, 
    preloadedState,
    applyMiddleware(thunk)
  )

  // Return store
  return store
}