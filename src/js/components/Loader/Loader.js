// Import dependencies
import React, { Component } from 'react'
import classNames from 'classnames'

// Import styles
import styles from './Loader.sass'

// Define class
class Loader extends Component {
  render() {
    // Construct classes from styles
    const classes = {
      Loader: classNames({
        // Always add loader class
        [styles[`Loader`]]: true,
        // Add dark class if requested
        [styles[`Loader--Dark`]]: this.props.style === 'dark'
      })
    }

    // Render loader
    return (
      <span className={classes.Loader}></span>
    )
  }
}

// Export component
export default Loader