// Import dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Import components
import Loader from '../../components/Loader/Loader'

// Import styles
import styles from './Viewer.sass'

// Define class
class Viewer extends Component {
  render() {
    // Get props
    const { loading, story } = this.props

    // Return loading markup
    if (loading) {
      return (
        <div className={styles.Viewer}>
          <div className={styles.Loader}>
            <Loader style="dark" />
          </div>
        </div>
      )
    }

    // Return markup
    return (
      <div className={styles.Viewer}>
        <div className={styles.Content}>
          {story.url && 
            <iframe className={styles.Iframe} src={story.url}/>
          }

          {!story.url &&
            <div className={styles.Post}>
              <h2 className={styles.Post__Title}>
                {story.title}
              </h2>
              
              <div className={styles.Post__Url}>
                <a href={`https://news.ycombinator.com/item?id=${story.id}`} target="_blank">
                  View on Hacker News &raquo;
                </a>
              </div>

              <div className={styles.Post__Text} dangerouslySetInnerHTML={{ __html: story.text }}></div>
            </div>
          }
        </div>
      </div>
    )
  }
}

// Map redux state to props
function mapStateToProps(state) {
  return {
    loading: state.viewer.loading,
    story: state.stories.current
  }
}

// Map actions to props
function mapDispatchToProps(dispatch) {
  return {}
}

// Connect with redux and export component
export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(Viewer)