// Import dependencies
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Import actions
import * as sidebarActionCreators from '../../actions/sidebar'

// Import components
import Logo from '../../components/Logo/Logo'

// Import styles
import styles from './Toggle.sass'

// Define class
class Toggle extends Component {
  render() {
    // Get props
    const { openDrawer } = this.props

    // Return markup
    return (
      <div className={styles.Toggle} onClick={openDrawer}>
        <div className={styles.Lines}>
          {[0,1,2].map(i => (
            <div key={i} className={styles.Line}></div>
          ))}
        </div>
      </div>
    )
  }
}

// Map redux state to props
function mapStateToProps(state) {
  return { }
}

// Map actions to props
function mapDispatchToProps(dispatch) {
  return bindActionCreators({...sidebarActionCreators}, dispatch)
}

// Connect with redux and export component
export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(Toggle)