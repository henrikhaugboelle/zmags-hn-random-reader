// Import dependencies
import React, { Component } from 'react'
import Numeral from 'numeral'
import Moment from 'moment'
import classNames from 'classnames'

// Import styles
import styles from './StoryItem.sass'

// Define class
class StoryItem extends Component {

  // Prevent navigation when a link is clicked
  preventNavigation(e) {
    e.stopPropagation()
  }
  
  render() {
    // Get props
    const { title, author, timestamp, score, url, isActive, onClick } = this.props

    // Format author karma
    const formattedAuthorKarma = Numeral(author.karma).format('0a')

    // Format timestamp
    const date = Moment(Date.now()).to(timestamp * 1000)

    // Render 
    return (
      <div className={classNames(styles.StoryItem, isActive && styles['StoryItem--Active'])} onClick={onClick}>
        <div className={styles.Date}>
          {date}
        </div>

        <div className={styles.Title}>
          {title}
        </div>

        <div className={styles.Info}>
          <div className={styles.Points}>
            {score} points
          </div>

          <div className={styles.Author}>
            By&nbsp;
            <a className={styles.Author__Name} href={`https://news.ycombinator.com/user?id=${author.id}`} target="_blank" onClick={::this.preventNavigation}>
              {author.id}
            </a>
            &nbsp;({formattedAuthorKarma} points)
          </div>

          <a className={styles.Url} href={url} target="_blank" onClick={::this.preventNavigation}>
            Visit url &raquo;
          </a>
        </div>
      </div>
    )
  }
}

// Export component
export default StoryItem