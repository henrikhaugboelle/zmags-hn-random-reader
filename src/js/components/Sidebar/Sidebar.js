// Import dependencies
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import classNames from 'classnames'

// Import actions
import * as sidebarActionCreators from '../../actions/sidebar'
import * as storiesActionCreators from '../../actions/stories'

// Import components
import Loader from '../../components/Loader/Loader'
import StoryItem from '../../components/StoryItem/StoryItem'

// Import styles
import styles from './Sidebar.sass'

// Define class
class Sidebar extends Component {

  handleSetStory(story) {
    // Set story
    this.props.setStory(story)

    // Close sidebar
    this.props.closeDrawer()
  }

  render() {
    // Get props
    let { loading, isOpen, closeDrawer, stories, story } = this.props

    // Construct classes
    const classes = {
      Sidebar: classNames({
        // Always apply sidebar class
        [styles['Sidebar']]: true,
        // Apply sidebar open class when drawer is open
        [styles['Sidebar--Open']]: isOpen
      })
    }

    // Mark stories active
    stories = stories.map(_story => ({
      ..._story,
      isActive: story && story.id === _story.id
    }))

    // Return loading markup
    if (loading) {
      return (
        <div className={classes.Sidebar}>
          <div className={styles.Backdrop} onClick={closeDrawer}></div>
          
          <div className={styles.Drawer}>
            <div className={styles.Loader}>
              <Loader />
            </div>
          </div>
        </div>
      )
    }

    // Return markup
    return (
        <div className={classes.Sidebar}>
          <div className={styles.Backdrop} onClick={closeDrawer}></div>
          
          <div className={styles.Drawer}>
            {stories.map((story, index) => (
              <StoryItem
                key={index}
                title={story.title}
                score={story.score}
                url={story.url}
                timestamp={story.time}
                author={story.author}

                isActive={story.isActive}

                onClick={() => ::this.handleSetStory(story)}
              />  
            ))}
          </div>
        </div>
    )
  }
}

// Map redux state to props
function mapStateToProps(state) {
  return {
    loading: state.sidebar.loading,
    isOpen: state.sidebar.drawer.open,
    stories: state.stories.all,
    story: state.stories.current
  }
}

// Map actions to props
function mapDispatchToProps(dispatch) {
  return bindActionCreators({...sidebarActionCreators, ...storiesActionCreators}, dispatch)
}

// Connect with redux and export component
export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(Sidebar)