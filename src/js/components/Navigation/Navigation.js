// Import dependencies
import React, { Component } from 'react'

// Import components
import Toggle from '../../components/Toggle/Toggle'
import Logo from '../../components/Logo/Logo'

// Import styles
import styles from './Navigation.sass'

// Define class
class Navigation extends Component {
  render() {
    // Get props
    const { loading } = this.props

    // Return markup
    return (
      <div className={styles.Navigation}>
        <div className={styles.Toggle}>
          <Toggle />
        </div>

        <a href="https://zmags.com" className={styles.Logo} target="_blank">
          <Logo />
        </a>
      </div>
    )
  }
}

export default Navigation