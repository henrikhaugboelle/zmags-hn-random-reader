// Import dependencies
import React, { Component } from 'react'

// Import images
import logo from './Logo.png'

// Import styles
import styles from './Logo.sass'

// Define class
class Logo extends Component {
  render() {
    return (
      <span className={styles.Logo}>
        <img className={styles.Source} src={logo} alt="Zmags HN Random Reader" />
      </span>
    )
  }
}

// Export component
export default Logo