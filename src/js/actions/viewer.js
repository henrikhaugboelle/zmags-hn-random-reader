// Define and export action constants
export const SET_LOADING = 'VIEWER_SET_LOADING'

// Set loading method
export function setLoading(loading) {
  return { type: SET_LOADING, data: { loading } }
}
