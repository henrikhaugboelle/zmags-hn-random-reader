// Import dependencies
import shuffle from 'lodash.shuffle'

// Import actions
import {
  setLoading as setSidebarLoading
} from '../actions/sidebar'

import {
  setLoading as setViewerLoading
} from '../actions/viewer'

// Import HN api
import HN from '../api'

// Define and export action constants
export const FETCH_RANDOM_STORIES = 'FETCH_RANDOM_STORIES'
export const SET_STORIES = 'SET_STORIES'
export const SET_STORY = 'SET_STORY'

// Set stories method
export function setStories(stories) {
  return { type: SET_STORIES, data: { stories }  }
}

// Set current story method
export function setStory(story) {
  return { type: SET_STORY, data: { story }  }
}

// Fetch random stories method
export function fetchRandomStories(url) {
  // Dispatch
  return async (dispatch) => {
    // Tell sidebar and viewer that we are loading
    dispatch(setSidebarLoading(true))
    dispatch(setViewerLoading(true))

    try {
      // Get top story ids
      const topStoryIds = await HN.getTopStories()

      // Shuffle story id
      const shuffledStoryIds = shuffle(topStoryIds)

      // Pick ten shuffled story ids
      const randomStoryIds = shuffledStoryIds.slice(0, 10)
      
      // Get story contents
      const storyPromises = randomStoryIds.map(async (storyId) => {
        // Get story
        const story = await HN.getStoryById(storyId)

        // Get author for story
        const author = await HN.getUserById(story.by)

        // Return story with assigned author
        return {
          ...story,
          author
        }
      })

      // Wait for stories to load
      const stories = await Promise.all(storyPromises)

      // Sort stories by score
      stories.sort((a, b) => a.score - b.score)
      
      // Put stories in redux store
      dispatch(setStories(stories))

      // Set first story as current
      dispatch(setStory(stories[0]))

      // Tell sidebar and viewer that we are no longer loading
      dispatch(setSidebarLoading(false))
      dispatch(setViewerLoading(false))

    } catch (e) {
      // An error occurred, log incident
      console.error(e)

      // Tell sidebar and viewer that we are no longer loading
      dispatch(setSidebarLoading(false))
      dispatch(setViewerLoading(false))
    }
  }
}