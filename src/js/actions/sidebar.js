// Define and export action constants
export const OPEN_DRAWER = 'SIDEBAR_OPEN_DRAWER'
export const CLOSE_DRAWER = 'SIDEBAR_CLOSE_DRAWER'
export const SET_LOADING = 'SIDEBAR_SET_LOADING'

// Open sidebar method
export function openDrawer() {
  return { type: OPEN_DRAWER  }
}

// Close sidebar method
export function closeDrawer() {
  return { type: CLOSE_DRAWER  }
}

// Set loading method
export function setLoading(loading) {
  return { type: SET_LOADING, data: { loading } }
}