// Import dependencies
import React, { Component } from 'react'

// Import styles
import styles from './App.sass'

// Define class
class App extends Component {
  render() {
    // Get props
    const { loading } = this.props

    // Return markup
    return (
      <div className={styles.App}>
        {this.props.children}
      </div>
    )
  }
}

export default App