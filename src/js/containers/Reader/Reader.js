// Import dependencies
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Import actions
import * as storiesActionCreators from '../../actions/stories'

// Import components
import Navigation from '../../components/Navigation/Navigation'
import Sidebar from '../../components/Sidebar/Sidebar'
import Viewer from '../../components/Viewer/Viewer'

// Import styles
import styles from './Reader.sass'

// Define class
class Reader extends Component {

  componentWillMount() {
    // Initiate random story fetching
    this.props.fetchRandomStories()
  }

  render() {
    // Get props
    const { loading } = this.props

    // Return markup
    return (
      <div className={styles.Reader}>
        <Navigation />
        <Sidebar />
        <Viewer />
      </div>
    )
  }
}


// Map redux state to props
function mapStateToProps(state) {
  return { }
}

// Map actions to props
function mapDispatchToProps(dispatch) {
  return bindActionCreators(Object.assign({}, storiesActionCreators), dispatch)
}

// Connect with redux and export component
export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(Reader)