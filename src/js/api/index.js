// Define HN API base url
const API_BASE = 'https://hacker-news.firebaseio.com/v0/'

// Generic api class for calling HN api
class Api {
  // Internal method for requests
  request(url, options = {}) {
    // Assemble options
    options = {
      ...options,
      headers: {}
    }

    // Remove leading slash
    url = url.replace(/^\//, '')

    // Add base url
    url = API_BASE + url
    
    // Execute request
    return fetch(url, options)
      .then(response => {
        if (response.status !== 200) { 
          throw new Error() 
        }

        return response.json()
      })
  }

  // Get method
  get(url, options = {}) {
    // Assemble options
    options = {
      ...options,
      method: 'GET'
    }

    // Execute request
    return this.request(url, options)
  }

  // Get topstories
  getTopStories() {
    return this.get('/topstories.json')
  }

  // Get story by id
  getStoryById(id) {
    return this.get(`/item/${id}.json`)
  }

  // Get user by id
  getUserById(id) {
    return this.get(`/user/${id}.json`)
  }
}

// Export instance of API
export default new Api
