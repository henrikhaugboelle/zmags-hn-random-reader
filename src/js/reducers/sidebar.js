// Import action constants
import {
  OPEN_DRAWER,
  CLOSE_DRAWER,
  SET_LOADING,
} from '../actions/sidebar'

// Create and export reducer
export default function(state = {
  // Initial state
  loading: true,
  drawer: {
    open: false
  }
}, action) {

  // Create new state, depending on action
  switch(action.type) {
    // Open sidebar
    case OPEN_DRAWER:
      return {
        ...state,
        drawer: {
          ...state.drawer,
          open: true
        }
      }

    // Close sidebar
    case CLOSE_DRAWER:
      return {
        ...state,
        drawer: {
          ...state.drawer,
          open: false
        }
      }

    // Set loading
    case SET_LOADING:
      return {
        ...state,
        loading: action.data.loading
      }

    default:
      return state
  }
}