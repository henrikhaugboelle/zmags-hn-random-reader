// Import dependencies
import { combineReducers } from 'redux'

// Import reducers
import sidebar from './sidebar'
import viewer from './viewer'
import stories from './stories'

// Combine and export reducers
export default combineReducers({
  sidebar,
  viewer,
  stories,
})