// Import action constants
import {
  SET_STORY,
  SET_STORIES,
} from '../actions/stories'

// Create and export reducer
export default function(state = {
  // Initial state
  all: [],
  current: null
}, action) {
  
  // Create new state, depending on action
  switch(action.type) {
    // Set all stories
    case SET_STORIES:
      return {
        ...state,
        all: action.data.stories
      }

    // Set current stories
    case SET_STORY:
      return {
        ...state,
        current: action.data.story
      }

    default:
      return state
  }
}