// Import action constants
import {
  SET_LOADING,
} from '../actions/viewer'

// Create and export reducer
export default function(state = {
  // Initial state
  loading: true,
}, action) {

  // Create new state, depending on action
  switch(action.type) {
    // Set loading
    case SET_LOADING:
      return {
        ...state,
        loading: action.data.loading
      }

    default:
      return state
  }
}