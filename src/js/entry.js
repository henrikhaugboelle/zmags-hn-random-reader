// Import dependencies
import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import Root from './root'

// Make global reference to root element
// (useful for handling global events)
window.__rootNode = document.getElementById('root')

// Render method
const render = (Component) => {
  // Actually render the component within the app container for hot reloading
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>,
    window.__rootNode
  )
}

// Call the render method
render(Root)

// If hot reloading is enabled
if (module.hot) {
  // Accept hot reloads for the root file
  module.hot.accept('./root', () => {
    // Rerender on hot reloading
    render(Root)
  })
}