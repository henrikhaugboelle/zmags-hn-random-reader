// Import global styles
import '../sass/_entry.scss'

// Import babel polyfill
import 'babel-polyfill'

// Import dependencies
import React from 'react'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

// Import configurer and reducers for redux
import { configureStore } from './store'
import reducers from './reducers'

// Configure redux store
const store = configureStore(reducers)

// Import containers
import App from './containers/App/App'
import Reader from './containers/Reader/Reader'

// Root element method bootstrapping redux,
// react-router and all of our own routes
const Root = () => (
  <Provider store={store}>
    <Router history={browserHistory} onUpdate={() => window.scrollTo(0, 0)}>
      <Route path="/" component={App} >
        <IndexRoute component={Reader} />
        <Route path=":id" component={Reader} />
      </Route>
    </Router>
  </Provider>
)

// Export root method
export default Root