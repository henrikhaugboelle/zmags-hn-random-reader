// Import dependencies
const Webpack = require('webpack')
const Autoprefixer = require('autoprefixer')

// Import common configuration
const common = require('./webpack.common')

// Create a copy of common configuration
let config = Object.assign({}, common)

// Set devtool to source map
config.devtool = 'source-map'

// Configure output
config.output = {
  path: '/',
  filename: '[name].[hash].js',
  publicPath: '/'
}

// Configure webpack development server
config.devServer =  {
  port: 3000,
  publicPath: '/',
  historyApiFallback: true,
  hot: true,
  noInfo: true,
  stats: {
    colors: true
  }
}

// Add hot reloading patch and webpack
// development server paths to entry point
config.entry.app.unshift(
  `react-hot-loader/patch`,
  `webpack-dev-server/client?http://localhost:${config.devServer.port}/`, 
  `webpack/hot/only-dev-server`
)

// Add development rules
config.module.rules = config.module.rules.concat([
  // Add sass rule which compiles sass,
  // runs the css through post css and autoprefixer,
  // uses css modules for nice separation,
  // and also resolves relative urls
  {
    test: /\.(scss|sass)$/,
    use: [
      {
        loader: 'style-loader'
      },
      {
        loader: 'css-loader',
        options: {
          modules: true,
          importLoaders: 3,
          localIdentName: '[local]_[hash:base64:7]'
        }
      },
      {
        loader: 'resolve-url-loader',
      },
      { 
        loader: 'postcss-loader',
        options: {
          plugins: [
            Autoprefixer({
              browsers: [
                'last 2 versions', 
                'ie >= 9', 
                'Android >= 2.3', 
                'ios >= 7'
              ]
            })
          ],
          sourceMap: true
        }
      },
      { 
        loader: 'sass-loader',
        options: {
          sourceMap: true
        }
      }
    ]
  }
])

// Add plugins to webpack configuration
config.plugins = config.plugins.concat([
  // Make sure that loaders are in development mode
  new Webpack.LoaderOptionsPlugin({
    debug: true
  }),
  // Hot module replacement
  new Webpack.HotModuleReplacementPlugin(),
  // Names are nice for debugging in development
  new Webpack.NamedModulesPlugin()
])

// Export development configuration
module.exports = config